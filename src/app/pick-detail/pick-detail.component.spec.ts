import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PickDetailComponent } from './pick-detail.component';

describe('PickDetailComponent', () => {
  let component: PickDetailComponent;
  let fixture: ComponentFixture<PickDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PickDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PickDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
