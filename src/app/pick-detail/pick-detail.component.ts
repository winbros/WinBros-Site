import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Pick } from '../classes/pick';
import { PickService } from '../pick.service';

@Component({
  selector: 'app-pick-detail',
  templateUrl: './pick-detail.component.html',
  styleUrls: ['./pick-detail.component.css']
})

export class PickDetailComponent implements OnInit {
  @Input() pick: Pick;
  picks: Pick[];

  constructor(
    private route: ActivatedRoute,
    private pickService: PickService,
    private location: Location
  ) {}

  ngOnInit() {
    this.getPick();
    this.getPicks();
  }

  getPick(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.pickService.getPick(id)
      .subscribe(pick => this.pick = pick);
  }

  getPicks(): void {
    this.pickService.getPicks().subscribe(picks => this.picks = picks);
  }

  picksByRow(rowNumber: number): Pick[] {
    const firstSelector = (rowNumber * 3) - 3;
    const lastSelector = (rowNumber * 3);

    const rowPickArray = this.picks.slice(firstSelector, lastSelector);
    return rowPickArray;
  }

}
