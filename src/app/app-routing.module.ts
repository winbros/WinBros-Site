import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainComponent } from './main/main.component';
import { PricesComponent } from './prices/prices.component';
import { PickComponent } from './pick/pick.component';
import { PickDetailComponent } from './pick-detail/pick-detail.component';

const routes: Routes = [
  { path: '', redirectTo: '/main', pathMatch: 'full' },
  { path: 'main', component: MainComponent },
  { path: 'prices', component: PricesComponent },
  { path: 'picks/:id', component: PickDetailComponent},
  { path: 'picks', component: PickComponent }
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})

export class AppRoutingModule { }
