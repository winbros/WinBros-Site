import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { MainComponent } from './main/main.component';
import { PricesComponent } from './prices/prices.component';
import { PickComponent } from './pick/pick.component';
import { PickDetailComponent } from './pick-detail/pick-detail.component';
import { MinipickComponent } from './minipick/minipick.component';
import { RefundComponent } from './refund/refund.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    PricesComponent,
    PickComponent,
    PickDetailComponent,
    MinipickComponent,
    RefundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
