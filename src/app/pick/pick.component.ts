import { Component, OnInit, AfterViewInit } from '@angular/core';

import { Pick } from '../classes/pick';

import { PickService } from '../pick.service';

@Component({
  selector: 'app-pick',
  templateUrl: './pick.component.html',
  styleUrls: ['./pick.component.css']
})
export class PickComponent implements OnInit, AfterViewInit {
  picks: Pick[];

  constructor(private pickService: PickService) { }

  ngOnInit() {
    this.getPicks();
  }

  ngAfterViewInit() {
    const s = document.createElement('script');
    s.type = 'text/javascript';
    s.src = 'assets/vendor/dzsparallaxer/dzsparallaxer.js';
    document.body.appendChild(s);
  }

  getPicks(): void {
    this.pickService.getPicks().subscribe(picks => this.picks = picks);
  }

  picksByRow(rowNumber: number): Pick[] {
    const firstSelector = (rowNumber * 3) - 3;
    const lastSelector = (rowNumber * 3);

    const rowPickArray = this.picks.slice(firstSelector, lastSelector);
    return rowPickArray;
  }
}
