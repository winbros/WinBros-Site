import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MinipickComponent } from './minipick.component';

describe('MinipickComponent', () => {
  let component: MinipickComponent;
  let fixture: ComponentFixture<MinipickComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MinipickComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MinipickComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
