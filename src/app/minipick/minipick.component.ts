import { Component, OnInit, Input } from '@angular/core';

import { Pick } from '../classes/pick';

@Component({
  selector: 'app-minipick',
  templateUrl: './minipick.component.html',
  styleUrls: ['./minipick.component.css']
})
export class MinipickComponent implements OnInit {
  @Input() pick: Pick;

  constructor() { }

  ngOnInit() {
  }

}
