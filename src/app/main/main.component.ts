import { Component, OnInit, AfterViewInit } from '@angular/core';

import { PickService } from '../pick.service';

import { Pick } from '../classes/pick';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})

export class MainComponent implements OnInit, AfterViewInit {
  picks: Pick[];

  constructor(private pickService: PickService) { }

  ngOnInit() {
    this.getPicks();
  }

  ngAfterViewInit() {
    const s = document.createElement('script');
    s.type = 'text/javascript';
    s.src = 'assets/vendor/dzsparallaxer/dzsparallaxer.js';
    document.body.appendChild(s);
  }

  getPicks(): void {
    this.pickService.getPicks().subscribe(picks => this.picks = picks);
  }

  picksByRow(rowNumber: number): Pick[] {
    const firstSelector = (rowNumber * 3) - 3;
    const lastSelector = (rowNumber * 3);

    const rowPickArray = this.picks.slice(firstSelector, lastSelector);
    return rowPickArray;
  }

}
