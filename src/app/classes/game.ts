export class Game {
  title: string;
  location: string;
  weather: string;
  time: string;
  score: string;
}
