import { Team } from './team';
import { Game } from './game';
export class Pick {
  id: number;
  sport: string;
  league: string;
  homeTeam: Team;
  awayTeam: Team;
  choice: Team;
  predictionTime: string;
  predictionBy: string;
  game: Game;
  correct: string;
  commentary: string;
}
