import { Component, OnInit } from '@angular/core';

import { PickService } from './pick.service';

import { Pick } from './classes/pick';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  masterPick: Pick[];

  constructor(private pickService: PickService) { }

  ngOnInit() {
    this.getPicks();
  }

  getPicks(): void {
    this.pickService.getPicks().subscribe(picks => this.masterPick = picks);
  }

  picksByRow(rowNumber: number): Pick[] {
    const firstSelector = (rowNumber * 3) - 3;
    const lastSelector = (rowNumber * 3);

    const rowPickArray = this.masterPick.slice(firstSelector, lastSelector);
    return rowPickArray;
  }
}
