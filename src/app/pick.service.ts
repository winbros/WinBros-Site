import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Pick } from './classes/pick';

const testPicks: Pick[] = [
  {
    id: 1,
    sport: 'a_football',
    league: 'NFL',
    game: {
      title: 'TB @ NE',
      time: '2/2/18 2:22 PM',
      score: '1 TB - 500 NE',
      location: 'your ass',
      weather: 'rainy'
    },
    homeTeam: {
      longName: 'Patriots',
      city: 'New England',
      shortCode: 'NE'
    },
    awayTeam: {
      longName: 'Bucaneers',
      city: 'Tampa Bay',
      shortCode: 'TB'
    },
    choice: {
      longName: 'Patriots',
      city: 'New England',
      shortCode: 'NE'
    },
    predictionTime: '1/1/18 1:11 PM',
    predictionBy: 'Scotti',
    correct: 'Correct',
    commentary: 'I don\'t want something too outrageous here obviously because we aren\'t like those other sites, but something.'
  }
];

@Injectable({
  providedIn: 'root'
})

export class PickService {

  constructor() { }

  getPicks(): Observable<Pick[]> {
    return of(testPicks);
  }

  getPick(id: number): Observable<Pick> {
    return of(testPicks.find(pick => pick.id === id));
  }

  getReducedPicks(limit: number): Observable<Pick[]> {
    const limitedPicks = testPicks.slice(testPicks.length - limit, testPicks.length);
    return of(limitedPicks);
  }
}
